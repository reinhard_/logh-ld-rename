#! /usr/bin/env sh

function rename {
    line=$1
    sfv=$(gzip -c < "$line" | tail -c8 | od -t x4 -N 4 -A n | tr -d ' ' | tr 'a-z' 'A-Z')
    renamed=$(echo "$line" | sed -E \
        -e "s/^(Legend of Galactic Heroes|logh eps) (eps|Eps)? ?([0-9]+).*$/Legend of the Galactic Heroes - \3 - Central Anime - [$sfv].avi/" \
        -e 's/ ([0-9]) / 00\1 /' \
        -e 's/ ([0-9]{2}) / 0\1 /'
    )
    mv "$line" "$renamed"
}

ls | while read line
do
    if [[ $line != $(basename $0) ]]; then
        rename "$line" &
    fi
done
